include("../src/AdaptiveDM.jl")
using .AdaptiveDM
using Plots

# Read the neCDF file
cd(@__DIR__)
scen = read_ProbStatScen_netcdf("total_ssp585_medium_confidence_values.nc")

# Plot AR6 secenario
plot(scen)

# Plot the distrubution of the first time step
dist = scen[1]
plot(dist)

# Draw sample and plot histogram
sample=rand(dist,10000)
histogram(sample)