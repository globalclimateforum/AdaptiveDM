include("../src/AdaptiveDM.jl")
using .AdaptiveDM
using StatsPlots
using Distributions

# Real-valued statis scenario
scen = StatScen([2020,2030,2040],[.2,.4,.5])
plot(scen)

# Probabalistic static scenario with Normal Distributions 
scen = StatScen([2020,2030,2040],map(Normal,[.2,.4,.5]))

# Probabalistic static scenario with and continuous nonparmetric distribution
d = ContNonparamDist([.0,1.0,2.0,3.0,4.0], [0,.1,.5,.9,1.0])
scen = StatScen([2020,2030,2040],[d,d,d],("Year", "Sea-level rise (m)"))
plot(scen)
