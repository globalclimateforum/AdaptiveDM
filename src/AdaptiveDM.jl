module AdaptiveDM

using DataInterpolations
using Distributions
using StatsPlots
using Plots.PlotMeasures

export ContNonparamDist
export StatScen
export read_ProbStatScen_netcdf
export TransLattice
export TransitionFunction
export lsgenerate_directfit
export lsgenerate_GBM_global
export lsgenerate_LVM
export combineTransLattice
export combineTransitionFunction
export delete_transition!
export states
export actions

export DP_back
export DP_forward
export print_opt_lattice
export plot_DP_result
export print_actions
export cost_function

#for tests 
export mean_var


export typeofState
export combineActionType
export combineActions 
export typeofAction
export states
export actions
export time_max
export time_steps
export combineProbStates
export nextProbStates
export next_states  

include("static_scenarios/ContNonparamDist.jl")
include("static_scenarios/StatScen.jl")
include("static_scenarios/io.jl")

# data strcutures
include("learning_scenarios/TransLattice.jl")
include("learning_scenarios/TransitionFunction.jl")
include("learning_scenarios/ContLearnScen.jl")

# generators
include("learning_scenarios/convert_ProbStatScen_to_DisreteLearnScen_directfit.jl")
include("learning_scenarios/GBM_non_stationary.jl")
include("learning_scenarios/GBM_stationary.jl")

#DP_back
include("dynamic_programming/dynamic_programming.jl")

end