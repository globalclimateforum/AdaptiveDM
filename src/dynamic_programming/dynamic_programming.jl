

#backwards recursion 
function DP_back(tl::TransitionFunction,cost::cost_function,discount::Float64)
    #create storage for output
    V=Dict{Tuple{Int64, Any}, Any}()
    pi=Dict{Tuple{Int64,Any}, Any}()
    t_before=0
    invest=0
    invest_opt=0
    maintain=0
    invest_total=0
    dam_total=0
    invest_total2=0
    dam_total2=0
    #backwards
    for t in reverse(time_steps(tl)) #iterate backwards
        t_delta=tl.ts[findfirst(isequal(t),tl.ts)+1]-t
        for s in states(tl,t)
            q=0 #optimal action
            value=Inf
            invest_total2=0
            dam_total2=0
            damage=cost.damage(s,t)
            for action in actions(tl,t,s) #iterate through all actions to find the cheapest one
                dam_total=damage*t_delta
                invest=cost.invest(s,action,t)
                maintain=cost.maintenance(s,t)*t_delta
                invest_total=invest+maintain
                costs=invest+maintain+damage*t_delta #adjust t_delta for each time step
                if t != time_steps(tl)[end]
                    for succ in next_states(tl,t,s,action)
                        costs=costs+succ[1]*V[(t_before,succ[2])][1] * (1/(1+discount)^t_delta) #bellman equation
                        invest_total=invest_total+succ[1]*V[(t_before,succ[2])][5] * (1/(1+discount)^t_delta)
                        dam_total=dam_total+succ[1]*V[(t_before,succ[2])][4] * (1/(1+discount)^t_delta)
                    end
                end
                #println("costs for t= $t, state= $s, action= $action are: $costs")
                if costs < value
                    q=action
                    value=costs
                    invest_opt=invest
                    invest_total2=invest_total
                    dam_total2=dam_total
                end
            end
            V[(t,s)]=(value,damage,invest_opt,dam_total2,invest_total2)
            pi[(t,s)]=q
        end
        t_before=t
    end
    return V, pi
end 

function DP_forward(tl::TransitionFunction,pi::Dict{Tuple{Int64,Any}, Any})
    #forward to read optimal actions under sea level rise 
    t1=time_steps(tl)[1]
    s1=states(tl,t1)[1] 
    #create new lattice
    opt = TransitionFunction{typeof(s1),typeof(pi[(t1,s1)])}(tl.ts;unit_time=tl.unit_time, unit_data=tl.unit_data) 
    t_before=0
    predecessors=[s1]
    successors=[] 
    list=[]
    for t in time_steps(tl) 
        if t==t1 
            add_transition!(opt,t,s1,pi[(t,s1)],next_states(tl,t,s1,pi[(t,s1)]))
        else          
            for s in predecessors
                for succ in next_states(tl,t_before,s,pi[(t_before,s)])
                    if succ[2] in list
                        nothing
                    else
                        add_transition!(opt,t,succ[2],pi[(t,succ[2])],next_states(tl,t,succ[2],pi[(t,succ[2])]))
                        push!(successors,succ[2])
                        push!(list,succ[2])
                    end 
                end 
            end
            predecessors=copy(successors)
            successors=[]
            list=[]
        end 
        t_before=t
    end
    return opt  
end 


