using Distributions
using StatsPlots

struct StatScen{T} 
    times::Vector{<:Real}
    values::Vector{T}
    units::Tuple{String,String}
end

# Constructors for using StatScen w/o units
StatScen(times::Vector{<:Real},values::Vector{T}) where {T} = StatScen{T}(times,values,("ND","ND"))

# Accessors
times(scen::StatScen) = scen.times
Base.values(scen::StatScen) = scen.values
units(scen::StatScen) = scen.units

function Base.getindex(scen::StatScen,index::Int64)
    scen.values[index]
end

function (scen::StatScen)(time)
    index=indexin(time,scen.times)[1]
    if index==nothing error("StatScen does not have time step $time") end
    scen.values[index]
end

###########
# Ploting #
###########

function RecipesBase.plot(scen::StatScen)
    plot(times(scen),values(scen),xlab=units(scen)[1],ylab=units(scen)[2])
end

# plot the median and the 5th and 95th percentiles
function RecipesBase.plot(scen::StatScen{T}) where {T<:Distribution}
    plot(times(scen),ccdf.(values(scen),.95),label="95th percentile",xlab=units(scen)[1],ylab=units(scen)[2])
    plot!(times(scen),ccdf.(values(scen),.5),label="median")
    plot!(times(scen),ccdf.(values(scen),.05),label="5th percentile")
end