# Reading the global probabalistic static scenrio from AR6
using NetCDF

function read_ProbStatScen_netcdf(path::String)
    ps=ncread(path,"quantiles")
    ts=convert(Vector{Int64},ncread(path,"years"))
    xss=convert.(Float64,ncread(path,"sea_level_change")[1,:,:])     
    num_ts = length(ts)
    ds = Vector{ContNonparamDist}(undef,num_ts)
    for i in 1:num_ts
        xs = xss[i,:] # values for the ith time step
        ds[i] = ContNonparamDist(xs,ps)
    end
    StatScen(ts,ds,("Year", "Sea-level rise (mm)"))
end
