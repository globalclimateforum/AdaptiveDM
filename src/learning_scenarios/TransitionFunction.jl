using RecipesBase

# Type constants
const TimeStep  = Int64
const Prob       = Float64
const ProbDist{T} = Vector{Tuple{Prob,T}}

"""
    TransitionFunction{T1,T2}

Data structure for representing discrete LS and discrete state spaces and transitions between them.

The data structure we have chosen follows closely the mathematical idea of a transition function ``f`` 
where the state at time ``s_{t+1}`` is a function of ``t``, the state at time t and the action ``a_t`` taken:

``f: T \\times S \\times A \\mapsto S where s_{t+1}=f(t, s_t, a_t)``

We capture this transition function (or rather the graph of this transition function) as 
as dictionary of dictionaries of dictionaries:

``f: T \\times S \\times A \\mapsto S \\equiv f: T \\mapsto ( S \\mapsto (A \\mapsto S ))``

"""
mutable struct TransitionFunction{T1,T2}
    ts::Vector{TimeStep} #this has one more final time step in the end compared to the data keys
    unit_time::String
    unit_data::String
    data::Dict{TimeStep, Dict{T1,Dict{T2,ProbDist{T1}}}}
end

TransitionFunction{T1,T2}(ts; unit_time="Time", unit_data="Data") where {T1,T2} = TransitionFunction(ts, unit_time, unit_data, Dict{TimeStep, Dict{T1,Dict{T2,ProbDist{T1}}}}())


"""
    add_transition!(tl::TransitionFunction{T1,T2},t::TimeStep,s::T1,a::T2,s_next::ProbDist{T1}) where {T1,T2}

Add transition to a TransitionFunction `tl`. 

A transition is defined by a start time index `t`, a start state `s` and an action `a`,
as well as the next state to which the action leads. Generally, the next state is a 
stochastic states represented by a discrete probability distribution (ProbDist). In 
the case of a deterministic transition, the next state is `[0,next_state]`.
"""
function add_transition!(tl::TransitionFunction{T1,T2},t::TimeStep,s::T1,a::T2,s_next::ProbDist{T1}) where {T1,T2}
    if !haskey(tl.data,t)
        tl.data[t] = Dict{T1,Dict{T2,ProbDist{T1}}}()
    end
    if !haskey(tl.data[t],s)
        tl.data[t][s] = Dict{T2,ProbDist{T1}}()
    end
    if !haskey(tl.data[t][s],a)
        tl.data[t][s][a] = s_next
    else
        error("Transition from TimeStep $t, State $s and Action $a already exists")
    end
end

function delete_transition!(tl::TransitionFunction{T1,T2},t::TimeStep,s::T1,a::T2) where {T1,T2}
    delete!(tl.data[t][s], a)    
end

# convenance function for adding transitions to deterministic states
function add_transition!(tl::TransitionFunction{T1,T2},t::TimeStep,s::T1,a::T2,s_next::T1) where {T1,T2}
    add_transition!(tl,t,s,a,[(1.0,s_next)])
end

#########################
# Advanced constructors #
#########################

function TransitionFunction{T1,T2}(time_steps::Vector{TimeStep},s₁::T1,actions_fun::Function,trans_fun::Function; unit_time="Time", unit_data="Data") where {T1,T2}
    tl=TransitionFunction{T1}{T2}(time_steps; unit_time=unit_time, unit_data=unit_data)
    states=[s₁]
    for t in time_steps 
        if t!= time_steps[end]
            for s in states
                actions = actions_fun(s)
                # println("Adding transitions for t=$t and s=$s:")
                for a in actions
                    s_next = trans_fun(s,a)        
                    #println("Adding transitions from (t=$t, s=$s, a=$a) => $s_next")
                    add_transition!(tl,t,s,a,s_next)
                end
            end
            states = next_states(tl,t)
        end
    end
    return tl
end

###################################
# Accessors: time steps and units #
###################################

"""
    unit_time(tl::TransitionFunction)

Returns the time unit.
"""
unit_time(tl::TransitionFunction) = return tl.unit_time

"""
    unit_data(tl::TransitionFunction)

Returns the data unit.
"""
unit_data(tl::TransitionFunction) = return tl.unit_data

"""
    time_steps(tl::TransitionFunction)

Return time steps as a vector that are used to store transition data (last time step excluded as no states exist there).
"""
time_steps(tl::TransitionFunction)=sort(collect(keys(tl.data)))

"""
    time(tl::TransitionFunction)

Return all time steps.
"""
time(tl::TransitionFunction)=tl.ts


#####################
# Accessors: states #
#####################

"""
    states(tl::TransitionFunction,t::TimeStep)

Return all possible states for time step `t`.
"""
function states(tl::TransitionFunction,t::TimeStep) 
    if haskey(tl.data,t) 
        collect(keys(tl.data[t]))
    else
        error("TransitionFunction does not have time step $t")
    end
end

##########################
# Accessors: next states #
##########################

"""
    next_states(tl::TransitionFunction,t::TimeStep)

Return a vector of all possible next states for time step `t` and optionally the current state `s`.
        
"""
function next_states(tl::TransitionFunction,t::TimeStep) 
    states = Vector()
     for s in keys(tl.data[t])
        states = unique(vcat(states, next_states(tl,t,s)))
        # println("n: ",next_states(tl,t,s))
    end
    return states
end

next_states(tl::TransitionFunction,t::TimeStep,s) = map(x->x[2],collect(Iterators.flatten(values(tl.data[t][s]))))

next_states(tl::TransitionFunction,t::TimeStep,s,a) = tl.data[t][s][a]

"""
    (tl::TransitionFunction)(t::TimeStep,s,a)

This is **the** transition function. Return a vector of next states for time step `t`, the current state `s`, and the action chosen `a`.
"""
(tl::TransitionFunction)(t::TimeStep,s,a) = tl.data[t][s][a]



all_states(tl) = [(t,s,a) for t in time_steps(tl) for s in states(tl,t) for a in actions(tl,t,s)]

###################################
# Accessors: actions              #
###################################

"""
    actions(tl::TransitionFunction,t::TimeStep,s)

Return the actions available at time step t and state s.
"""
actions(tl::TransitionFunction,t::TimeStep,s) = collect(keys(tl.data[t][s]))

############
# Printing #
############

"""
    RecipesBase.print(tl::TransitionFunction)

TBW
"""
function RecipesBase.print(tl::TransitionFunction)
    println(typeof(tl), ": Time steps: ", time_steps(tl))
    for t in time_steps(tl)
        println("t=$t")
        for s in sort(states(tl,t))
            for a in actions(tl,t,s) # actions cannot be sorted because the type is Union{Char,Nothing} and Nothing can not be compared to Char
                println("      $s, $a => ", next_states(tl,t,s,a))
            end
        end
    end
end
RecipesBase.show(tl::TransitionFunction) = print(tl)
RecipesBase.display(tl::TransitionFunction) = print(tl)

function print_actions(tl::TransitionFunction)
    unit_data=tl.unit_data
    println(typeof(tl), ": Time steps: ", time_steps(tl))
    for t in time_steps(tl)
        println("t=$t")
        for s in sort(states(tl,t))
            for a in actions(tl,t,s) # actions cannot be sorted because the type is Union{Char,Nothing} and Nothing can not be compared to Char
                if false in iszero.(a)
                    println("    if $unit_data = $s, do $a")
                end
            end
        end
    end
end

function print1(tl::TransitionFunction)
    for from in [(t,s,a) for t in time_steps(tl) for s in states(tl,t) for a in actions(tl,t,s)]
        println(from, " => ", tl(from...))
    end
end

function print2(tl::TransitionFunction)
    for (t,s,a) in [(t,s,a) for t in time_steps(tl) for s in states(tl,t) for a in actions(tl,t,s)]
        println(t,s,a, " => ", tl(t,s,a))
    end
end

function print3(tl::TransitionFunction)
    for (t,s,a) in all_states(tl)
        println((t,s,a), " => ", tl(t,s,a))
    end
end

############
# Plotting #
############

using Plots
"""
    RecipesBase.plot(tl::TransitionFunction)

TBW
"""
function RecipesBase.plot(tl::TransitionFunction)
    g=plot()
    for (t,sₜ,aₜ) in all_states(tl)
        trans = next_states(tl,t,sₜ,aₜ)
        for (p,s_next) in trans
            if p==1.0 label="$aₜ" else label="($aₜ,$p)" end
            plot!(g,
                [t,tl.ts[findfirst(isequal(t),tl.ts)+1]],
                [sₜ, s_next],
                color="blue") #,annotations = ([t+.5], [(s+s_next)/2], text(label, 8)),color=:blue)
            scatter!(g,
                [t,tl.ts[findfirst(isequal(t),tl.ts)+1]],[sₜ, s_next],
                color="black",
                xlabel=unit_time(tl),
                ylabel=unit_data(tl))
        end
    end
    plot(g,legend=false)
end


##################################
# Combining transition functions #
##################################

# utility functions

function combineProbDist(ps_a::ProbDist, ps_b::ProbDist)
    combProbDist = ProbDist{Tuple{typeof(ps_a[1][2]),typeof(ps_b[1][2])}}()
    for (s_a,s_b) in [(a,b) for a in ps_a for b in ps_b]
        s_comb = s_a[1]*s_b[1],(s_a[2],s_b[2])
        # println("",s_a,"*",s_b,"=", s_comb)
        push!(combProbDist,s_comb)
    end
    return combProbDist
end

function combineActionType(t_a,t_b)
    if t_a == Nothing 
        return t_b
    elseif t_b == Nothing 
        return t_a
    else
        return Tuple{t_a,t_b}
    end
end

function flatten(a_a,a_b)
    return Tuple(vcat(a_a...,a_b...))
end

function combineActions(a_a,a_b)
    if a_a == nothing 
        return a_b
    elseif a_b == nothing 
        return a_a
    else
        return (a_a,a_b)
    end
end

typeof_state(tl::TransitionFunction) = typeof(tl).parameters[1]

typeof_action(tl::TransitionFunction) = typeof(tl).parameters[2]

# utility functions

"""
    Base.:*(tl_a::TransitionFunction,tl_b::TransitionFunction)

TBW
"""
Base.:*(tl_a::TransitionFunction,tl_b::TransitionFunction) = combineTransitionFunction(tl_a,tl_b)

"""
    combineTransitionFunction(tl_a::TransitionFunction,tl_b::TransitionFunction)

TBW
"""
function combineTransitionFunction(tl_a::TransitionFunction,tl_b::TransitionFunction)
    if time_steps(tl_b)!=time_steps(tl_a)
        error("TransitionFunctions to be combined do not have the same number of time steps")
    end
    tl_comb = TransitionFunction{Tuple{typeof_state(tl_a),typeof_state(tl_b)}}{combineActionType(typeof_action(tl_a),typeof_action(tl_b))}(tl_a.ts;unit_time=tl_a.unit_time, unit_data=tl_a.unit_data*", "*tl_b.unit_data)
    for t in time_steps(tl_a)
        # println("t$t")
        for s in [(a,b) for a in states(tl_a,t) for b in states(tl_b,t)]
            # println("  s=$s")
            for (a_a,a_b) in [(a,b) for a in actions(tl_a,t,s[1]) for b in actions(tl_b,t,s[2])]
                ss = combineProbDist(next_states(tl_a,t,s[1],a_a),next_states(tl_b,t,s[2],a_b))
                aa = combineActions(a_a,a_b)
                # println("    ", aa," => ", ss)
                add_transition!(tl_comb,t,s,aa,ss)
            end
        end
    end
    return tl_comb
end

mutable struct cost_function
    damage::Function 
    invest::Function 
    maintenance::Function 
end

