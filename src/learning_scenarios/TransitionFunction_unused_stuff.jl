using RecipesBase

#############
# Printing  #
#############


#for dike lattice that cannot be sorted by states
function print_lattice(tl)
    for t in times(tl)
        if t==(times(tl)[time_max(tl)])
            nothing
        else
            println("t$t")
            for s in states(tl,t)
                println("  s=$s")
                for a in actions(tl,t,s) # actions cannot be sorted because the type is Union{Char,Nothing} and Nothing can not be compared to Char
                    println("    ", a," => ", nextProbStates(tl,t,s,a))
                end
            end
        end
    end
end

function print_opt_lattice(tl)
    #dict={}
    #new dict with
    
    for t in times(tl)
    dict= Dict{typeof(states(tl,1)[1]), Action}()
        println("t$t")
        for s in states(tl,t)
            for a in actions(tl,t,s)
                if a[1]>0
                    dict[s]=a
                end
            end
        end 
        
        
        #for s in states(tl,t)
        for a in union(values(dict))
            println(" do $a")
            keys=[k for (k,v) in dict if v==a]
            for k in keys
                println("   if state $k")
            end
        end
        #end
        

    end
end


############
# Plotting #
############

using Plots
function RecipesBase.plot(tl::TransLattice)
    g=plot()
    Δt=times(tl)[2]-times(tl)[1]
    for t in times(tl)
        if t==time_max(tl)
            nothing
        else
            for s in states(tl,t)
                for a in actions(tl,t,s)
                    trans = nextProbStates(tl,t,s,a)
                    for (p,s_next) in trans
                        if p==1.0 label="$a" else label="($a,$p)" end
                        plot!(g,[t,t+Δt],[s, s_next],color="blue")#,annotations = ([t+.5], [(s+s_next)/2], text(label, 8)),color=:blue)
                        scatter!(g,[t,t+Δt],[s, s_next],color="black",xlabel="Year",ylabel="Sea level rise (mm)")
                    end
                end
            end
        end
    end
    plot(g,legend=false)
end


################
# Input/Output #
################

#save lattice to CSV doc 
function save_opt_lattice(tl,doc,column_name,Value)
    #dict={}
    #new dict with
    df = DataFrame(CSV.File(doc))
    
    #new column
    df[!, column_name] .= 0.0
    df[!, column_name*"_flex"] .= 0.0
    df[1,column_name]=Value[1,states(tl,1)[1]] #value of optimal decision rule today

    for i in 2:nrow(df)#-10
        if haskey(tl.data[df[i,"t"]],(df[i,"slr"],(df[i,"dike"],df[i,"flex"])))
            #print(actions(tl,df[i,"t"],(df[i,"s1"],(df[i,"s21"],df[i,"s22"])))[1][1])
            df[i,column_name]=actions(tl,df[i,"t"],(df[i,"slr"],(df[i,"dike"],df[i,"flex"])))[1][1]
            df[i,column_name*"_flex"]=actions(tl,df[i,"t"],(df[i,"slr"],(df[i,"dike"],df[i,"flex"])))[1][2]
        end 
    end 
    CSV.write(doc, df)
end