"""
    lsgenerate_GBM_local

Function that generates a continuous learning scenario *ContLearnScen* from a static 
probabalistic scenrio *StatProbScen* using a Geometric Brownian Motion Model with 
dynamic volatility over time.
"""
function lsgenerate_LVM(stat_scen::StatScen{ContNonparamDist})
    #return array of mean and variance for every time step    
    SLR_mean=[mean_var(stat_scen,t)[1] for t in 1:length(stat_scen.times)]
    SLR_var=[mean_var(stat_scen,t)[2] for t in 1:length(stat_scen.times)]
    SLR_mean2=[mean_var(stat_scen,t)[3] for t in 1:length(stat_scen.times)]
    println("check")
    ps = (
        s0 =stat_scen.values[1].support[54],
        ts = [0:1:length(stat_scen.times);],
        #S=broadcast(lognormal,[.3:.1:1.2;],fill(0.001, I)))
        #S=broadcast(lognormal,[.3:.1:1.2;],[0.001:0.001:0.01;]))
        S_mean=SLR_mean,
        S_var=SLR_var,
        S_mean2=SLR_mean2)
        
    lvm_2 = ps2lvm(ps)

    #calculate up and down movement for LVM
    Delta_t=1
    μ=lvm_2.μs
    σ=lvm_2.σs
    println(μ)
    println(σ)
    
    #use Jarrow and Rudd
    up=[]
    down=[]

    for i in 1:length(stat_scen.times)
        push!(up,exp((μ[i]-σ[i]^2/2)*Delta_t+σ[i]*sqrt(Delta_t)))
        push!(down,exp((μ[i]-σ[i]^2/2)*Delta_t-σ[i]*sqrt(Delta_t)))
    end

   
    
    #create Learning Scenario/Lattice
    p=0.5
    L = TransLattice{Float64,Nothing}()

    #position nodes with up and down values
    m=2
    succ=[]
    succ_new=[]
    for t in 1:length(stat_scen.times)
        if t == length(stat_scen.times)
            nothing
        elseif t == 1
            #first node on median
            position=stat_scen.values[1].support[54]
            position_succ1=position*up[t]
            position_succ2=position*down[t]
            append!(succ_new,position_succ1,position_succ2)
            addTrans!(L,convert(Int64,stat_scen.times[t]),position,nothing,[(p,position_succ1),(1-p,position_succ2)])
            
        else
            for j in 1:m 
                for s in succ
                #add positions to TransLattice/learning scenario
                    addTrans!(L,convert(Int64,stat_scen.times[t]),s,nothing,[(p,s*up[t]),(1-p,s*down[t])])
                    append!(succ_new,s*up[t])
                    append!(succ_new,s*down[t])
                end
            end
            m=m+1
        end
        succ=succ_new
        succ=unique(succ)
        succ_new=[]
    end
   return L
        

end




#functions to fit CRR GBM and LVM Learning Scenario


gbm_sample(s0, μ, σ, t) = s0 * exp( (μ-((σ^2)/2)) * t + σ * sqrt(t)*rand(Normal(0,1)) )
#gbm_sample(s0, μ, σ, t) = s0 * exp( (μ-((σ^2)/2)) * t + σ * rand(Normal(0,sqrt(t))) )
gbm_sample(gbm,t)         = gbm_sample(gbm.s0, gbm.μ, gbm.σ, t)

function gbm_sample_path(s0, μ , σ, N::Int64; t0::Int32, Δt::Int32, R::Int64=1)
    steps=trunc(Int,(N-t0)/Δt)   #calculate steps based on N (t_max) and delta t
    s = zeros(steps+1, R)     
    for r in 1:R
        s[1,r] = s0
        for n in 1:steps
            s[n+1,r] = gbm_sample(s[1,r], μ, σ, n)#*Δt+t0)
        end
    end
    return s
end

function lvm_sample(s0, μs , σs, ts, t)
    I = length(ts)  #V: changed to ts which is +1, therefore last s_t can be deleted
    s_t = s0
    i=2
    while (t>ts[i-1] && i<=I)
        #print(i)
        s_t = gbm_sample(s_t, μs[i-1], σs[i-1], ts[i]-ts[i-1])   #V:here delta t instead of t
        i += 1
        #println(s_t)
    end  
    
    #s_t = gbm_sample(s_t, μs[i], σs[i], t-ts[i-1])
    return s_t
end

lvm_sample(lvm, t) = lvm_sample(lvm.s0, lvm.μs , lvm.σs, lvm.ts, t)

function lvm_sample_path(s0, μs , σs, ts)
    N = length(ts)
    s = zeros(N)
    s[1] = s0 #V: start with s0 as first data point
    for n in 2:N
      s[n] = gbm_sample(s[n-1], μs[n-1], σs[n-1], ts[n]-ts[n-1]) #v: add -ts[n-1]  
    end
    return s
end
lvm_sample_path(lvm) = lvm_sample_path(lvm.s0, lvm.μs , lvm.σs, lvm.ts)


function ps2lvm_drift(S_i_mean,S_i_minus_1_mean,Δt)
    μ = (1/Δt)*log(S_i_mean/S_i_minus_1_mean)
end

function ps2lvm_drift(ps)
    s0,ts,S_mean,S_mean2,S_var = ps.s0,ps.ts,ps.S_mean,ps.S_mean2,ps.S_var # unpack variables for better readability and closeness to math
    I = length(S_mean)
    μs = zeros(I)
    μs[1] = log(S_mean[1]/s0)/(ts[2]-ts[1])
    for i in 2:I
        μs[i] = ps2lvm_drift(S_mean[i],S_mean[i-1],ts[i+1]-ts[i])
    end
    return μs
end

function ps2lvm_volatility(S_i_mean,S_i_var,S_i_minus_1_mean,S_i_minus_1_mean2,Δt,μ)
    #print(mean1)
    fraction1 = S_i_var/exp(2*μ*Δt)
    fraction2=(fraction1+S_i_minus_1_mean^2)/S_i_minus_1_mean2
    #print(fraction2)
    σ = sqrt(log(fraction2)/Δt)
    return σ
end

function gbm_volatility(s0,S_t_var,t,μ)
    fraction = S_t_var/(s0^2*exp(2*μ*t))
    σ = sqrt(log(fraction+1)/t)
    return σ
end

function ps2lvm_volatility(ps,μs)
    s0, ts, S_mean, S_mean2, S_var = ps.s0, ps.ts, ps.S_mean, ps.S_mean2, ps.S_var
    I = length(S_mean)
    σs = zeros(I)
    σs[1] = gbm_volatility(s0,S_mean[1],ts[2]-ts[1],μs[1])
    #print(σs[1])
    for i in 2:I
        σs[i] = ps2lvm_volatility(S_mean[i],S_var[i],S_mean[i-1],S_mean2[i-1],ts[i+1]-ts[i],μs[i])
    end
    return σs
end

function ps2lvm(ps)
    μs = ps2lvm_drift(ps)
    lvm = (
        s0 = ps.s0,
        ts = ps.ts,
        μs = μs,
        σs = ps2lvm_volatility(ps,μs)
    )
end