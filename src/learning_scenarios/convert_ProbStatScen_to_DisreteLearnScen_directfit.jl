"""
    lsgenerate_directfit

Function that generates a disctrete learning scenario *DiscLearnScen* from a static 
probabalistic scenrio *StatProbScen* following the direct fit method introduced
by Völz and Hinkel (2023) https://doi.org/10.1029/2023EF003662

This includes skip or shift function

"""


function lsgenerate_directfit(ss::StatScen,p,skip,shift) 

    if skip==true && shift>0
        print("only skip or shift possible")
        return 0
    end
    
    L = TransitionFunction{Float64,Nothing}(ss.times;unit_time="Year", unit_data="Sea level rise (mm)")
    
    #position nodes (set state values) according to QF and probabilities of nodes (binomial)
    m=shift #determine number of states
    for t in 1:length(ss.times)
        if t == length(ss.times)
            nothing
        else
            for j in 0:m
                binomial_top=0
                binomial_low=0
                for k in 0:j
                    #print("k",k,"\n")
                    D= Binomial(m,p)
                    binomial_top=binomial_top+ pdf(D,k)
                end
                for k in 0:(j-1)
                    D= Binomial(m,p)
                    binomial_low=binomial_low+ pdf(D,k)
                end 
                if j==0
                    binomial_low=0
                end
                if m==0
                    binomial_top=1
                end
                if binomial_top>1
                    binomial_top=1
                end
                
                prob=mean([binomial_low,binomial_top]) #prob of node
                prob_quantile=findmin(broadcast(abs,ss.values[t].p.-prob))[2] #find closest quantile value to prob of node

                position=ss.values[t].support[prob_quantile] #read SLR value from this quantile value via QF
                


                #we need positions of both successor states, m is next time period
                if skip==true
                    m_succ=m+2
                else
                    m_succ=m+1
                end
                #first successor has same j, higher m
                binomial_top_succ1=0
                binomial_low_succ1=0
                for k in 0:j
                    #print("k",k,"\n")
                    D= Binomial(m_succ,p)
                    binomial_top_succ1=binomial_top_succ1+ pdf(D,k)
                end
                for k in 0:(j-1)
                    D= Binomial(m_succ,p)
                    binomial_low_succ1=binomial_low_succ1+ pdf(D,k)
                end 
                if j==0
                    binomial_low_succ1=0
                end
                if m_succ==0
                    binomial_top_succ1=1
                end
                if binomial_top_succ1>1
                    binomial_top_succ1=1
                end
                
                #second successor has higher j
                binomial_top_succ2=0
                binomial_low_succ2=0
                j_succ2=j+1
                for k in 0:j_succ2
                    #print("k",k,"\n")
                    D= Binomial(m_succ,p)
                    binomial_top_succ2=binomial_top_succ2+ pdf(D,k)
                end
                for k in 0:(j_succ2-1)
                    D= Binomial(m_succ,p)
                    binomial_low_succ2=binomial_low_succ2+ pdf(D,k)
                end 
                if j_succ2==0
                    binomial_low_succ2=0
                end
                if m_succ==0
                    binomial_top_succ2=1
                end
                if binomial_top_succ2>1
                    binomial_top_succ2=1
                end
                
                prob_succ1=mean([binomial_low_succ1,binomial_top_succ1]) #prob of node
                prob_quantile_succ1=findmin(broadcast(abs,ss.values[t].p.-prob_succ1))[2] #find closest quantile value to prob of node
                position_succ1=ss.values[t+1].support[prob_quantile_succ1] #read SLR value from quantile of sample
                prob_succ2=mean([binomial_low_succ2,binomial_top_succ2]) #prob of node
                prob_quantile_succ2=findmin(broadcast(abs,ss.values[t].p.-prob_succ2))[2] #find closest quantile value to prob of node
                position_succ2=ss.values[t+1].support[prob_quantile_succ2] #read SLR value from quantile of sample

                #add positions to TransLattice/learning scenario
                if skip==true
                    add_transition!(L,convert(Int64,ss.times[t]),position,nothing,[(p*p,position_succ1),(2*p*(1-p),position_succ2),((1-p)*(1-p),position_succ2)])
                else
                    add_transition!(L,convert(Int64,ss.times[t]),position,nothing,[(p,position_succ1),(1-p,position_succ2)])
                   
                
                end
            end
            if skip==true
                m=m+2
            else
                m=m+1
            end
        end
    end
    return L
end
