using RecipesBase

# Type constants
const TimeIndex  = Int64
const State      = Any
const Prob       = Float64
const Action     = Any
const ProbDist{T} = Vector{Tuple{Prob,T}}



#=
Data structure for representing discrete LS and discrete state spaces and transitions between them.

The data structure we have choosen follows closely the mathematical idea of a transition function ``f`` 
where the state at time ``s_{t+1}`` is a function of ``t``, the state at time t and the action ``a_t`` taken:

``f: T \times S \times A \mapsto S where s_{t+1}=f(t, s_t, a_t)``

We capture this transition function (or rather the graph of this transition function) as 
as dictionary of dictionaries of dictionaries:

``f: T \\times S \\times A \\mapsto S \\equiv f: T \\mapsto ( S \\mapsto (A \\mapsto S ))``

=#
TransLattice{T1,T2} = Dict{TimeIndex, Dict{T1,Dict{T2,ProbDist{T1}}}}

# Convenience constructor
TransLattice(T1::DataType,T2::DataType) = TransLattice{T1}{T2}()

"""
Function for adding a transition to a TransLattice `tl`. 

A transition is defined by a start time index `t`, a start state `s` and an action `a`,
as well as the next state to which the action leads. Generally, the next state is a 
stochastic states represented by a discrte probability distribution (ProbDist). In 
the case of a deterministic transition, the next state is `[0,next_state]`.
"""
function addTrans!(tl::TransLattice,t::TimeIndex,s::State,a::Action,s_next::ProbDist)
    # where {T<:Vector{Tuple{Any,Any}}}
    tf = tl
    if !haskey(tf,t)
        tf[t] = Dict()
    end
    if !haskey(tf[t],s)
        tf[t][s] = Dict()
    end
    if !haskey(tf[t][s],a)
        tf[t][s][a] = s_next
    else
        tf[t][s][a] = [tf[t][s][a];s_next]
        #error("Transition from TimeIndex $t, State $s and Action $a already exists")
    end
end

#############
# Accessors #
#############

"""
Returns all time steps as a vector
"""
time_list(tl::TransLattice)=sort([k for k in keys(tl)])

"""
Returns the maximum time steps of TransLattice `tl`.
"""
time_max(tl::TransLattice) = time_list(tl)[end]

"""
Returns a vector of all possible next states given time index `t` and optionally the current state `s`.
"""
nextPossibleStates(tl::TransLattice,t::TimeIndex,s::State) = map(x->x[2],collect(Iterators.flatten(values(tl[t][s]))))

function nextPossibleStates(tl::TransLattice,t::TimeIndex) 
    ss = Vector{State}()
    for s in keys(tl[t])
        ss = unique(vcat(ss, nextPossibleStates(tl,t,s)))
        # print("n",nextPossibleStates(tl,t,s))
    end
    return ss
end

function states(tl::TransLattice,t::TimeIndex) 
    if t <= time_max(tl)
        collect(keys(tl[t]))    
    elseif t == time_max(tl)+(time_list(tl)[2]-time_list(tl)[1])
       nextPossibleStates(tl,t)  
    else
        error("TransLattice does not have time index $t")
    end
end

actions(tl::TransLattice,t::TimeIndex,s::State) = collect(keys(tl[t][s]))

nextProbStates(tl::TransLattice,t::TimeIndex,s::State,a::Action) = tl[t][s][a]

function typeofState(tl::TransLattice)
    return typeof(states(tl,time_list(tl)[1])[1])
end

function typeofAction(tl::TransLattice)
    s = states(tl,time_list(tl)[1])[1]
    a = actions(tl,time_list(tl)[1],s)[1]
    return typeof(a)
end

################
# Constructors #
################

function TransLattice(T1::DataType, T2::DataType, s_1::State,actionsFun::Function,transFun::Function,start::Integer,step::Integer, n::Integer)
    tl=TransLattice{T1}{T2}()
    states=[s_1]
    for t in start:step:n+step
        for s in states
            actions = actionsFun(s)
                 #println("Adding transitions for t=$t and s=$s:")
            for a in actions
                if t==n+step
                    s_next=[(0.0,s)]# Vector{Tuple{Prob,T}}
                    addTrans!(tl,t,s,a,s_next)
                else 
                    s_next = transFun(s,a)
                    
                 #println("Adding transitions from t=$t and s=$s under a=$a => $s_next")
                    addTrans!(tl,t,s,a,s_next)
                end 
            end
        end
        states = nextPossibleStates(tl,t)
    end
    return tl
end

function TransLattice(T1::DataType, T2::DataType, s_1::State,actionsFun::Function,transFun::Function,ts::Vector{TimeIndex})
    tl=TransLattice{T1}{T2}()
    states=[s_1]
    for t in ts
        for s in states
            actions = actionsFun(s)
                 #println("Adding transitions for t=$t and s=$s:")
            for a in actions
                if t==ts[end]
                    s_next=[(0.0,s)]# Vector{Tuple{Prob,T}}
                    addTrans!(tl,t,s,a,s_next)
                else 
                    s_next = transFun(s,a)
                    
                 #println("Adding transitions from t=$t and s=$s under a=$a => $s_next")
                    addTrans!(tl,t,s,a,s_next)
                end 
            end
        end
        states = nextPossibleStates(tl,t)
    end
    return tl
end

# A second function that infers the datatypes of state and action from s_1 and actionsFun
function TransLattice(s_1::State,actionsFun::Function,transFun::Function,start::Integer,step::Integer, n::Integer)
    typeofState  = typeof(s_1)
    typeofAction = typeof(actionsFun(s_1)[1])
    TransLattice(typeofState, typeofAction, s_1, actionsFun, transFun, start, step, n)
end

# A second function that infers the datatypes of state and action from s_1 and actionsFun
function TransLattice(s_1::State,actionsFun::Function,transFun::Function,ts::Vector{TimeIndex})
    typeofState  = typeof(s_1)
    typeofAction = typeof(actionsFun(s_1)[1])
    TransLattice(typeofState, typeofAction, s_1, actionsFun, transFun,ts)
end

#############
# Printing  #
#############

"""
    RecipesBase.print(tl::TransLattice)

TBW
"""
function RecipesBase.print(tl::TransLattice)
    for t in time_list(tl)
        if t==time_max(tl)
            nothing
        else
            println("t$t")
            for s in sort(states(tl,t))
                println("  s=$s")
                for a in actions(tl,t,s) # actions cannot be sorted because the type is Union{Char,Nothing} and Nothing can not be compared to Char
                    println("    ", a," => ", nextProbStates(tl,t,s,a))
                end
            end
        end
    end
end

#for dike lattice that cannot be sorted by states
function print_lattice(tl)
    for t in time_list(tl)
        if t==(time_list(tl)[time_max(tl)])
            nothing
        else
            println("t$t")
            for s in states(tl,t)
                println("  s=$s")
                for a in actions(tl,t,s) # actions cannot be sorted because the type is Union{Char,Nothing} and Nothing can not be compared to Char
                    println("    ", a," => ", nextProbStates(tl,t,s,a))
                end
            end
        end
    end
end

function print_opt_lattice(tl)
    #dict={}
    #new dict with
    for t in time_list(tl)
    dict= Dict{typeof(states(tl,time_list(tl)[1])[1]), Action}()
        println("t$t")
        for s in states(tl,t)
            for a in actions(tl,t,s)
                if a>0
                    dict[s]=a
                end
            end
        end 
        #for s in states(tl,t)
        for a in union(values(dict))
            println(" do $a")
            keys=[k for (k,v) in dict if v==a]
            for k in keys
                println("   if state $k")
            end
        end
        #end
        

    end
end


############
# Plotting #
############

using Plots
function RecipesBase.plot(tl::TransLattice)
    g=plot()
    t_delta=time_list(tl)[2]-time_list(tl)[1]
    for t in time_list(tl)
        if t==time_max(tl)
            nothing
        else
            for s in states(tl,t)
                for a in actions(tl,t,s)
                    trans = nextProbStates(tl,t,s,a)
                    for (p,s_next) in trans
                        if p==1.0 label="$a" else label="($a,$p)" end
                        plot!(g,[t,t+t_delta],[s, s_next],color="blue")#,annotations = ([t+.5], [(s+s_next)/2], text(label, 8)),color=:blue)
                        scatter!(g,[t,t+t_delta],[s, s_next],color="black",xlabel="Year",ylabel="Sea level rise (mm)")
                    end
                end
            end
        end
    end
    plot(g,legend=false)
end


################
# Input/Output #
################

#save lattice to CSV doc 
function save_opt_lattice(tl,doc,column_name,Value)
    #dict={}
    #new dict with
    df = DataFrame(CSV.File(doc))
    
    #new column
    df[!, column_name] .= 0.0
    df[!, column_name*"_flex"] .= 0.0
    df[1,column_name]=Value[1,states(tl,1)[1]] #value of optimal decision rule today

    for i in 2:nrow(df)#-10
        if haskey(tl[df[i,"t"]],(df[i,"slr"],(df[i,"dike"],df[i,"flex"])))
            #print(actions(tl,df[i,"t"],(df[i,"s1"],(df[i,"s21"],df[i,"s22"])))[1][1])
            df[i,column_name]=actions(tl,df[i,"t"],(df[i,"slr"],(df[i,"dike"],df[i,"flex"])))[1][1]
            df[i,column_name*"_flex"]=actions(tl,df[i,"t"],(df[i,"slr"],(df[i,"dike"],df[i,"flex"])))[1][2]
        end 
    end 
    CSV.write(doc, df)
end

#plot optimal actions as decision tree over time
function plot_DP_result(tl::TransLattice)
    s_pos=20
    g=plot(size=(3000,1000),xticks=time_list(tl),yaxis=nothing,margin=10mm,ylabel="Optimal actions",xlabel="Time",grid=false)
    for t in time_list(tl)
        for (slr,dike) in states(tl,t)
            for a in actions(tl,t,(slr,dike))
                if a>0
                    trans = nextProbStates(tl,t,(slr,dike),a)
                    for (p,s_next) in trans
                        if p==1.0 label="$a" else label="($a,$p)" end
                        plot!(g,[t-0.25],[s_pos],color="black")#,series_annotations="if state=$s do $a")
                        annotate!(g,[t-0.25],[s_pos],text(" if SLR ≤$slr cm \n & dike=$dike\n → increase dike by $a cm",:black, :left, 8))
                        #plot!(g,[t,t+1],[s, s_next],annotations = ([t+.5], [(s+s_next)/2], text(label, 8)),color=:blue)
                    end
                end
            end
            s_pos=s_pos+20
        end
        s_pos=20
    end
    plot(g,legend=false)
end

# Type constants
const TimeIndex  = Int64
const State      = Any
const Prob       = Float64
const Action     = Any
const ProbDist{T} = Vector{Tuple{Prob,T}}
const ProbStates{T} = Vector{Tuple{Prob,T}}


#cartesian product of two lattices
function combineProbStates(ps_a::ProbDist, ps_b::ProbDist)
    combProbStates = ProbStates{Tuple{typeof(ps_a[1][2]),typeof(ps_b[1][2])}}()
    for (s_a,s_b) in [(a,b) for a in ps_a for b in ps_b]
        s_comb = s_a[1]*s_b[1],(s_a[2],s_b[2])
        # println("",s_a,"*",s_b,"=", s_comb)
        push!(combProbStates,s_comb)
    end
    return combProbStates
end

function combineProbStates(ps_a::ProbDist, ps_b::ProbDist, ps_c::ProbDist)
    combProbStates = ProbStates{Tuple{typeof(ps_a[1][2]),typeof(ps_b[1][2]),typeof(ps_c[1][2])}}()
    for (s_a,s_b,s_c) in [(a,b,c) for a in ps_a for b in ps_b for c in ps_c]
        s_comb = s_a[1]*s_b[1]*s_c[1],(s_a[2],s_b[2],s_c[2])
        # println("",s_a,"*",s_b,"=", s_comb)
        push!(combProbStates,s_comb)
    end
    return combProbStates
end

function combineActionType(t_a,t_b)
    if t_a == Nothing 
        return t_b
    elseif t_b == Nothing 
        return t_a
    else
        return Tuple{t_a,t_b}
    end
end

function combineActionType(t_a,t_b,t_c)
    if t_a == Nothing && t_b == Nothing 
        return t_c
    elseif t_b == Nothing && t_c == Nothing 
        return t_a
    elseif t_a == Nothing && t_c == Nothing 
        return t_b
    else
        return Tuple{t_a,t_b,t_c}
    end
end

function combineActions(a_a,a_b)
    if a_a == nothing 
        return a_b
    elseif a_b == nothing 
        return a_a
    else
        return (a_a,a_b)
    end
end

function combineActions(a_a,a_b,a_c)
    if (a_a == nothing) && (a_b == nothing) 
        return a_c
    elseif a_b == nothing && a_c == nothing 
        return a_a
    elseif a_a == nothing && a_c == nothing 
        return a_b
    else
        return (a_a,a_b,a_c)
    end
end

function combineTransLattice(tl_a::TransLattice,tl_b::TransLattice)
    if time_max(tl_b)!=time_max(tl_a) error("TL to be combined do not have the same number of time steps")     end
    tl_comb = TransLattice(Tuple{typeofState(tl_a),typeofState(tl_b)},combineActionType(typeofAction(tl_a),typeofAction(tl_b)))
    for t in time_list(tl_a)
        # println("t$t")
        j=1
        for s in [(a,b) for a in states(tl_a,t) for b in states(tl_b,t)]
            # println("  s=$s")
            for (a_a,a_b) in [(a,b) for a in actions(tl_a,t,s[1]) for b in actions(tl_b,t,s[2])]
                ss = combineProbStates(nextProbStates(tl_a,t,s[1],a_a),nextProbStates(tl_b,t,s[2],a_b))
                aa = combineActions(a_a,a_b)
                # println("    ", aa," => ", ss)
                
                addTrans!(tl_comb,t,s,aa,ss)
            end
        end
    end
    return tl_comb
end

function combineTransLattice(tl_a::TransLattice,tl_b::TransLattice,tl_c::TransLattice) 
    if time_max(tl_b)!=time_max(tl_a) || time_max(tl_c)!=time_max(tl_a) || time_max(tl_c)!=time_max(tl_b) error("TL to be combined do not have the same number of time steps")     end
    tl_comb = TransLattice(Tuple{typeofState(tl_a),typeofState(tl_b),typeofState(tl_c)},combineActionType(typeofAction(tl_a),typeofAction(tl_b),typeofAction(tl_c)))
    for t in time_list(tl_a)
        j=1
        for s in [(a,b,c) for a in states(tl_a,t) for b in states(tl_b,t) for c in states(tl_c,t)]
            for (a_a,a_b,a_c) in [(a,b,c) for a in actions(tl_a,t,s[1]) for b in actions(tl_b,t,s[2]) for c in actions(tl_c,t,s[3])]
                ss = combineProbStates(nextProbStates(tl_a,t,s[1],a_a),nextProbStates(tl_b,t,s[2],a_b),nextProbStates(tl_c,t,s[3],a_c))
                aa = combineActions(a_a,a_b,a_c)
                addTrans!(tl_comb,t,s,aa,ss)
            end
        end
    end
    return tl_comb
end