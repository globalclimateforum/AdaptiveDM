"""
    lsgenerate_GBM_global

Function that generates a continuous learning scenario *ContLearnScen* from a static 
probabalistic scenrio *StatProbScen* using a Geometric Brownian Motion Model with 
constant volatility.
"""
function lsgenerate_GBM_global(stat_scen::StatScen{ContNonparamDist})
    #fit GBM to last time step
    mean,var,mean2=mean_var(stat_scen,length(stat_scen.times))
    t_intervals=stat_scen.times[2]-stat_scen.times[1]
    #t_intervals=t-1
    μ=log(mean/stat_scen.values[1].support[54])/t_intervals #use median value of first time step
    sigma=sqrt(log(var/((stat_scen.values[1].support[54])^2*exp(2*t_intervals*μ))+1)/t_intervals)
    println(μ)
    println(sigma)

    #Jarrow and Rudd
    Delta_t=1#QF.t[2]-QF.t[1]
    up=exp((μ-sigma^2/2)*Delta_t+sigma*sqrt(Delta_t))
    down=exp((μ-sigma^2/2)*Delta_t-sigma*sqrt(Delta_t))
    p=0.5
    
    #create Learning Scenario/Lattice
    L = TransLattice{Float64,Nothing}()

    #position nodes with up and down values
    m=2
    succ=[]
    succ_new=[]
    for t in 1:length(stat_scen.times)
        if t == length(stat_scen.times)
            nothing
        elseif t == 1
            #first node on median
            position=stat_scen.values[1].support[54]
            position_succ1=position*up
            position_succ2=position*down
            append!(succ_new,position_succ1,position_succ2)
            addTrans!(L,convert(Int64,stat_scen.times[t]),position,nothing,[(p,position_succ1),(1-p,position_succ2)])
            
        else
            for j in 1:m 
                for s in succ
                #add positions to TransLattice/learning scenario
                    addTrans!(L,convert(Int64,stat_scen.times[t]),s,nothing,[(p,s*up),(1-p,s*down)])
                    append!(succ_new,s*up)
                    append!(succ_new,s*down)
                end
            end
            m=m+1
        end
        succ=succ_new
        succ=unique(succ)
        succ_new=[]
    end
    return L
end


#sample mean and variance of Static_scen in last time step for GBM Learning Scenario
function mean_var(stat_scen::StatScen{ContNonparamDist},t)
    sample = rand(stat_scen[t],10000)
    m=mean(sample)
    v=var(sample)
    m2=mean(map(x-> x^2, sample))
    
    return m, v, m2
end 

